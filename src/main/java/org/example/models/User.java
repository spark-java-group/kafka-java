package org.example.models;

import lombok.*;
import static org.example.utils.Utility.*;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private int id;
    private String name;
    private String address;
    private int pinCode;
    private String email;
    private long evtDateTime = getCurrentTimeInUTC();

    @Override
    public String toString() {
        return writeValueAsString(this);
    }
}
