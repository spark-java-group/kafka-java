package org.example.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class ProduceFromFile {
    public static void main(String[] args) throws Exception {
        Properties runProperties = new Properties();
        runProperties.load(new FileReader(args[0]));
        Properties properties = new Properties();
        properties.put("bootstrap.servers", runProperties.getProperty("bootstrap.servers"));
        properties.put("client.id", runProperties.getProperty("client.id"));
        properties.put("key.serializer", runProperties.getProperty("key.serializer"));
        properties.put("value.serializer", runProperties.getProperty("value.serializer"));
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);
        String topic=runProperties.getProperty("produce.topic");

        Files.readAllLines(Paths.get(args[1]))
                .stream()
                .forEach(
                     data -> {
                         ProducerRecord<String, String> msg = new ProducerRecord<>(topic, String.valueOf(Math.random() * 100000), data);
                         try {
                             RecordMetadata recordMetadata = producer.send(msg).get();
                             System.out.println(data);
                         } catch (Exception e) {
                             throw new RuntimeException(e);
                         }
                     }
                );
    }
}
