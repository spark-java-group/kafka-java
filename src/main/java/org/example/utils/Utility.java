package org.example.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Utility {
    private static  ObjectMapper mapper;
    private static final String MSG_EVT_DATE_FORMAT="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final SimpleDateFormat sdf = new SimpleDateFormat(MSG_EVT_DATE_FORMAT);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MSG_EVT_DATE_FORMAT, Locale.getDefault());

    static {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
    public static String writeValueAsString(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public static Object readValueAsObject(String json, Class clazz) throws Exception {
        try {
            return mapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            return clazz.getDeclaredConstructor().newInstance();
        }
    }

    public static Object readValueAsObject(String json, TypeReference clazz) throws Exception {
        try {
            return mapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            return null;
        }
    }
    public static long convertDateToMillis(String dateTime) {
        LocalDateTime localTime = LocalDateTime.parse(dateTime, formatter);
        return localTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static String convertMillisecondToDateTime(Long timeInMillis) {
        LocalDateTime ldt = LocalDateTime.ofInstant(Instant.ofEpochMilli(timeInMillis), ZoneId.systemDefault());
        return ldt.format(formatter);
    }
    /**
     * Extracts the time as seconds of day, from 0 to 24 * 60 * 60 - 1.
     * Returns: the second-of-day equivalent to this time
     */
    public static int getCurrentTimeInSecond() {
        LocalTime localTime = LocalTime.now();
        return localTime.toSecondOfDay();
    }

    /**
     * Get current time in UTC
     */
    public static long getCurrentTimeInUTC(){
        return Instant.ofEpochMilli(System.currentTimeMillis())
                .atZone(ZoneOffset.UTC)
                .toInstant().toEpochMilli();
    }
}
