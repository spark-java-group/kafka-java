package org.example;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.example.models.User;

import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import static org.example.utils.Utility.getCurrentTimeInUTC;

/**
 * Hello world!
 *
 */
public class App 
{
    private static  KafkaProducer<String, String> producer;
    private static  String topic="demo-topic";

    public static void main( String[] args ) throws Exception{
        Properties runProperties = new Properties();
        runProperties.load(new FileReader(args[0]));

        Properties properties = new Properties();
        properties.put("bootstrap.servers", runProperties.getProperty("bootstrap.servers"));
        properties.put("client.id", runProperties.getProperty("client.id"));
        properties.put("key.serializer", runProperties.getProperty("key.serializer"));
        properties.put("value.serializer", runProperties.getProperty("value.serializer"));
        producer = new KafkaProducer<>(properties);

        for(int i=0;i<Integer.valueOf(runProperties.getProperty("producer.msg.limit"));i++){
            Thread.sleep(1000);
            User user = User.builder()
                    .address("d-1108,TCG, Hinjewadi")
                    .id((int) ((Math.random() * 100) % 50))
                    .email("lkapse" + (int) ((Math.random() * 100) % 50) + "@gmail.com")
                    .name("lucky" + (int) ((Math.random() * 100) % 50))
                    .pinCode((int) ((Math.random() * 10000)))
                    .evtDateTime(getCurrentTimeInUTC())
                    .build();
            String data = user.toString();
            ProducerRecord<String, String> msg = new ProducerRecord<>(topic,String.valueOf(Math.random()*100000),data);
            RecordMetadata recordMetadata = producer.send(msg).get();
            System.out.println(data);
        }

    }
}
