START
C:/big-data/kafka_2.12-3.1.1/bin/windows/zookeeper-server-start.bat C:/big-data/kafka_2.12-3.1.1/config/zookeeper.properties
C:/big-data/kafka_2.12-3.1.1/bin/windows/kafka-server-start.bat C:/big-data/kafka_2.12-3.1.1/config/server.properties

%KAFKA_HOME%/bin/windows/zookeeper-server-start.bat %KAFKA_HOME%/config/zookeeper.properties
%KAFKA_HOME%/bin/windows/kafka-server-start.bat %KAFKA_HOME%/config/server.properties
-----------------------------------------------------------------------------------------------------------------------------
STOP 

C:/big-data/kafka_2.12-3.1.1/bin/windows/zookeeper-server-stop.bat C:/big-data/kafka_2.12-3.1.1/config/zookeeper.properties
C:/big-data/kafka_2.12-3.1.1/bin/windows/kafka-server-stop.bat C:/big-data/kafka_2.12-3.1.1/config/server.properties
-----------------------------------------------------------------------------------------------------------------------------

COMMANDS
%KAKFKA_HOME%/kafka-topics.bat --list --bootstrap-server localhost:9092
kafka-topics.bat --version 
kafka-console-consumer.bat --topic demo-topic --from-beginning --bootstrap-server localhost:9092
kafka-console-consumer.bat --topic out-topic --from-beginning --bootstrap-server localhost:9092
kafka-topics.bat --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic out-topic